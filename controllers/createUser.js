/* pegar os elementos do DOM*/

const inputNome = document.querySelector("#cadastro-input-nome");
const inputSenha = document.querySelector("#cadastro-input-senha");
const inputEmail = document.querySelector("#cadastro-input-email");
const inputDataDeNascimento = document.querySelector("#cadastro-input-data-de-nascimento");
const aviso = document.querySelector("#aviso-preencher");

const createBtn = document.querySelector("#create-btn");

createBtn.addEventListener("click", async (event) => {
    event.preventDefault();

    const nome = inputNome.value;
    const senha = inputSenha.value;
    const email = inputEmail.value;
    const dataDeNascimento = inputDataDeNascimento.value;

    if(nome === "" || senha === "" || email === "" || dataDeNascimento === ""){
        aviso.classList.remove("invisible");
    }else{
        await criarRegistro(nome, senha, email, dataDeNascimento);
        aviso.classList.add("invisible");
    }
})


const criarRegistro = async (nome, senha, email, dataDeNascimento) => {
    try {
        const response = await fetch("http://localhost:3000/cadastrar", {
            method: 'POST',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                Nome: `${nome}`, 
                Senha: `${senha}`,  
                Email: `${email}`,  
                DataDeNascimento: `${dataDeNascimento}`
            })
        });

        const content = await response.json();

        return content;


    } catch (error) {
        console.log(error);
    }
}