# Atividade Html/Css/Formulario

Repositorio para as atividade de Html e Css, e a de POST(formulario)

## Instalacao

Clonar o repositorio com o seguinte comando

```bash
git clone https://gitlab.com/michelmschorr/atividade-html-css.git main
```

Instalar o json-server: 

```bash
npx install json-server
```
## Uso

Primeiro rodamos o arquivo index.html com a extensao do live-server do vscode, para abrir o arquivo no local host

Depois rodamos o json-server com o seguinte comando
```bash
npx json-server --watch db.json
```

Abrimos a porta cadastro do servidor no browser

Vamos para a pagina html, a landing page do civitas, preenchemos os campos dos formularios e clicamos em cadastrar. Isso vai cadastrar o usuario no banco de dados.